import {autoReducer} from 'fast-redux-reducer'
import LayoutConfiguration from '../containers/LayoutConfiguration/LayoutConfiguration.reducer'
import Layout from '../containers/Layouts/Layouts.reducer'
export const reducersObject = {
    LayoutConfiguration,
    Layout
}
// config what store data save to localStorage
export function customSlicer () {
    return (state) => {
        const {
            layoutConfiguration,
            layoutConfigurationEditMode,
            layoutConfigurationShowComponents,
            layoutSavedLayouts,
            layoutSavedPages,
        } = state
        return {
            layoutConfiguration,
            layoutConfigurationEditMode,
            layoutConfigurationShowComponents,
            layoutSavedLayouts,
            layoutSavedPages,
        }
    }
}
export default autoReducer(reducersObject)