import { fork } from 'redux-saga/effects'
import Routes from '../routes/Routes.saga'
import LayoutConfiguration from '../containers/LayoutConfiguration/LayoutConfiguration.saga'
export default function * sagaMiddleware () {
 yield fork(Routes)
 yield fork(LayoutConfiguration)
}