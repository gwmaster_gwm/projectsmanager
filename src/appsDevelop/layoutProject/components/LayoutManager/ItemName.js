import React from 'react'
import './ItemName.css'
const ItemName = ({name , visible = true, index , layout , setLayout}) => {
    const changeName = ()  => {
        // update name
        var newName = prompt("Change grid item name:", name);
        if (newName) {
            let newLayout = [...layout]
            newLayout[index] = {...newLayout[index], i: newName}
            setLayout(newLayout)
        }
    }
    if(!visible){
        return null
    }
    return (
         <div className='item-name' onClick={changeName}>{name}</div>
    )
}

export default ItemName