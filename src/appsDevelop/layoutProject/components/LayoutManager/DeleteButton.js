import React, { useEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import './DeleteButton.css'
const DeleteButton = ({onClick , visible}) => {
  if(!visible) return null
  return (<div onClick={() => onClick()} className='delete-button'><HighlightOffIcon/></div>)
}
DeleteButton.propTypes = {
    onClick : PropTypes.func,
    visible : PropTypes.bool
}
DeleteButton.defaultProps = {
    onClick : () => {},
    visible : true
}

export default DeleteButton