import React, {useEffect, useState} from 'react';

import '../dynamicDND/dynamicDND.stories.css'
import Switch from '@material-ui/core/Switch';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import List from '../material-ui/List'
import Header from "../material-ui/Header";
import AppBar from  '../material-ui/AppBar'
import Logo from '../basic/Logo'

export default {
    title: 'layoutProject',
    component : LayoutManager
};
const updateLayoutData = [{"i":"logo","x":6,"y":0,"w":1,"h":24,"moved":false,"static":false},{"i":"navigation","x":6,"y":24,"w":1,"h":90,"moved":false,"static":false,"minW":1,"maxW":2},{"i":"header","x":0,"y":0,"w":6,"h":13,"moved":false,"static":false},{"i":"content","x":0,"y":13,"w":6,"h":100,"moved":false,"static":false}]
const updateLayoutData2 = [{"i":"logo","x":2,"y":13,"w":4,"h":93,"moved":false,"static":false},{"i":"navigation","x":6,"y":13,"w":1,"h":93,"moved":false,"static":false,"minW":1,"maxW":2},{"i":"header","x":0,"y":0,"w":7,"h":13,"moved":false,"static":false},{"i":"name_1581802217549","x":0,"y":13,"w":2,"h":93,"moved":false,"static":false},{"i":"name_1581802229517","x":0,"y":106,"w":7,"h":18,"moved":false,"static":false}]
const testLayoutData = [{"i":"logo","x":0,"y":0,"w":1,"h":13,"c":"Logo","p":{"url":"https://marketingland.com/wp-content/ml-loads/2015/11/content-marketing-idea-lightbulb-ss-1920-800x450.jpg"},"moved":false,"static":false},{"i":"navigation","x":0,"y":13,"w":1,"h":132,"moved":false,"static":false,"c":"List"},{"i":"header","x":1,"y":0,"w":6,"h":13,"moved":false,"static":false,"c":"AppBar"},{"i":"content","x":1,"y":13,"w":6,"h":132,"moved":false,"static":false,"c":"Logo","p":{"url":"https://webcomicms.net/sites/default/files/clipart/135001/happy-smile-135001-6608522.jpg"}}]
// layout
import {TextField} from "@material-ui/core";
import LayoutManager , {updateLayout , addItem , clearLayout} from "./LayoutManager";
import ComponentsList from "./ComponentsList";
/*
    Mast add components that will use !!
 */
const COMPONENTS = {
    Logo,
    AppBar,
    List,
    Header,
}
// const of components that have dynamic params {param : defaultValue }
// TODO : build dynamic params system
const COMPONENTS_PARAMS = {
    Logo : {
        c : 'Logo',
        url : 'https://marketingland.com/wp-content/ml-loads/2015/11/content-marketing-idea-lightbulb-ss-1920-800x450.jpg',
        dialog : true
    },
    LogoSmile : {
        c : 'Logo',
        p : { url: 'https://webcomicms.net/sites/default/files/clipart/135001/happy-smile-135001-6608522.jpg' }
    },
    LogoCool: {
        c : 'Logo',
        p : { url : 'https://i.dlpng.com/static/png/6972879_preview.png' }
    },
    AppBar,
    List,
    Header
}
export const LayoutManagerTest = () => {
    // layout is an array of objects, see the demo for more complete usage
    const [isEditor , setIsEditor] = useState(true);
    const [isPreview , setIsPreview] = useState(false)
    const [showComponents , setShowComponents] = useState(true)
    const [layout , setLayout] = useState([])
    const [cols , setCols] = useState(7)
    useEffect(() => {
        setLayout(testLayoutData)
    }, [])
    return (
        <div className="grid-container">
            <div className="controllTools">
                Edit Mode :
                <Switch checked={isEditor}  onChange={() => setIsEditor(!isEditor)} value="checkedB" color="primary" inputProps={{ 'aria-label': 'primary checkbox' }}/>
                Is Preview :
                <Switch checked={isPreview}  onChange={() => setIsPreview(!isPreview)} value="checkedB" color="primary" inputProps={{ 'aria-label': 'primary checkbox' }}/>
                <br/>
                Show Components :
                <Switch checked={showComponents}  onChange={() => setShowComponents(!showComponents)} value="checkedB" color="primary" inputProps={{ 'aria-label': 'primary checkbox' }}/>
                <TextField style={{width : 30}} id="standard-number" label="Cols" type="number" value={cols} onChange={({target : {value}}) => setCols(value) }/>
                <button onClick={()=>setLayout(clearLayout(layout))}>Clear</button>
                <button onClick={()=>setLayout(addItem(layout))}>add Item</button>
                <button onClick={()=>setLayout([])}>Delete</button>
                <br/>
                <button onClick={()=>setLayout(updateLayout(layout , testLayoutData))}>Load Layout</button>
                <button onClick={()=>setLayout(updateLayout(layout , updateLayoutData))}>Update Layout</button>
                <button onClick={()=>setLayout(updateLayout(layout , updateLayoutData2))}>Update Layout2</button>
                <br/>
                <br/>
                {/*<TextField id="standard-number" label="Row Height" type="number" value={rowHeight} onChange={({target : {value}}) => setRowHeight(value) }/>*/}
                <TextareaAutosize className='log-text' rowsMax={4} value={ JSON.stringify(layout)}/>
            </div>
            <div className="componentsList">
                Components:
                <ComponentsList components={COMPONENTS_PARAMS}/>
            </div>
            <div className="react-grid">
                <LayoutManager isEditor={isEditor} showComponents={showComponents} isPreview={isPreview} cols={cols} layout={layout} setLayout={setLayout} components={COMPONENTS} componentsParams={COMPONENTS_PARAMS} />
            </div>
        </div>
    )
}

