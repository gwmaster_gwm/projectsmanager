import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import PropTypes from 'prop-types';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));


const ComponentsList = ({components = {}}) => {
  const classes = useStyles();
  const drag = (componentName) => (event) => {
    event.dataTransfer.setData("component-name", componentName);
  }
  return (
      <div className={classes.root}>
        <List component="Component" aria-label="component list">
          {Object.keys(components).map((name , index) => (
              <ListItem key={"c_"+index} button draggable="true" onDragStart={drag(name)}>
                <ListItemText >{name}</ListItemText>
              </ListItem>
          ))}

        </List>
      </div>
  )
}

ComponentsList.propTypes = {
  components : PropTypes.object
}
ComponentsList.defaultProps = {
  components : {}
}

export default ComponentsList