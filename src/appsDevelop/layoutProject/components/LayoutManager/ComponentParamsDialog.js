import React, {useEffect, useState} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';

// TODO : make dialog params dinamic by passed params fields
const ComponentParamsDialog = ({openPopUp , onClose}) => {
    const [open, setOpen] = React.useState(false);
    const [url , setUrl] = useState('');
    useEffect(() => {
        setOpen(openPopUp != null)
        if(openPopUp){
            const {params} = openPopUp
            setUrl(params.url)
        }
    }, [openPopUp])
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = (isApply) =>() => {
        if(isApply) {
            const {index, layout, setLayout, componentName} = openPopUp;
            let newLayout = [...layout]
            newLayout.splice(index, 1, {...newLayout[index], c: componentName, p: {url}});
            setLayout(newLayout)
        }
        setOpen(false);
    };
    if(openPopUp == null){
        return null
    }
    const {index, layout} = openPopUp;
    const gridItemName = layout[index].i
    return (
            <Dialog open={open} onClose={handleClose(false)} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Add Logo to {gridItemName}</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="url"
                        value={url}
                        onChange={ ({target : {value}})=> setUrl(value)}
                        label="url"
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose(false)} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={handleClose(true)} color="primary">
                        Add
                    </Button>
                </DialogActions>
            </Dialog>
    );
}
ComponentParamsDialog.propTypes = {
    isOpen : PropTypes.object
}
ComponentParamsDialog.defaultProps = {
    isOpen : null
}

export default ComponentParamsDialog