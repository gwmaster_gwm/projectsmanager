import React, { useEffect, useState, Fragment , useImperativeHandle } from 'react'
import PropTypes from 'prop-types';
import _ from "lodash";
import ClearButton from "./ClearButton";
import DeleteButton from "./DeleteButton";
import ItemName from "./ItemName";
import ComponentParamsDialog from "../LayoutManager/ComponentParamsDialog";
import '../../styles/react-grid-layout.css'
import '../../styles/react-resizable.css'

import RGL, {WidthProvider , Responsive} from "react-grid-layout";
const ReactGridLayout = WidthProvider(RGL);
//const ReactGridLayout = WidthProvider(Responsive)

/**
 updateLayout
**/
export const updateLayout = (layout , newLayout) => {
  let hashMapLayout = {}
  layout.forEach(item => hashMapLayout[item.i] = item);
  let mergetLayout = newLayout.map(item => {
    let mergetItem = {...hashMapLayout[item.i] , ...item};
    return mergetItem
  });
  return mergetLayout
}

/**
 clearLayout
**/
export const clearLayout = (layout) => {
  return [...layout].map(item => {
    const {c ,p ,...other} = item;
    return other
  })
}

/**
 add layout Item
**/
export const addItem = (layout = [] , name = 'name_'+new Date().getTime() , x = 0 , y =  Infinity , w = 1 , h = 10 ) => {
  return layout.concat([{i: name, x, y, w, h}])
}

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
export const  LayoutManager = (props) => {
  // props
  const {layout , setLayout , isEditor , isPreview , showComponents  , margin , componentsParams , components , ...layoutProps} = props
  const [componentHashMap , setComponentHashMap] = useState({})
  const [openPopUp , setOpenPopUp] = useState(null)
  const [initlized , setInitlized] = useState(false)
  const defaultProps = {
    className: isPreview ? 'preview' : "layout",
    isDraggable: isEditor,
    isResizable: isEditor,
    onLayoutChange: newLayout => setLayout(updateLayout(layout , newLayout)),
    margin : showComponents ? [0,0] : margin,
    ...layoutProps
  };
  useEffect(() => {
    // hash map to get component by his name
    const newComponentHashMap = {};
    Object.keys(components).map(key => newComponentHashMap[key] = components[key])
    setComponentHashMap(newComponentHashMap)
    setInitlized(true)
  }, [])
  /*
     Drag and drop
   */
  const onDrop = (event) => {
    const {currentTarget : {dataset : {gridcellname}}, dataTransfer} = event
    let componentName = dataTransfer.getData("component-name").split("\n")[0];
    let newLayout = [...layout]
    let index = _.findIndex(newLayout, {i: gridcellname});
    // check if component have dynamic params to show popUp
    if(componentsParams[componentName].dialog){
      setOpenPopUp({
        params : componentsParams[componentName],
        componentName,
        index,
        layout : newLayout,
        setLayout
      })
      return
    }
    // check if basic component or extra params
    let componentObject = componentsParams[componentName]
    if(componentObject.p){
      // add components and props
      newLayout.splice(index, 1, {...newLayout[index] , ...componentObject});
    }else{
      delete newLayout[index].p // delete previews component props
      // add basic component
      newLayout.splice(index, 1, {...newLayout[index] , c : componentName});
    }
    setLayout(newLayout)
  }
  const allowDrop = (ev) => {
    if(!isEditor){
      return false
    }
    ev.preventDefault();
  }
  /*

   */
  const onClear = (name) => () => {
    var index = _.findIndex(layout, {i : name});
    let newLayout = [...layout];
    delete newLayout[index].c; // delete component
    delete newLayout[index].p; // delete props
    setLayout(newLayout)
  }
  const onDelete = (name) => () => {
    var index = _.findIndex(layout, {i : name});
    let newLayout = [...layout]
    newLayout.splice(index, 1)
    setLayout(newLayout)
  }
  const renderComponent = (props , index) => {
    const {i , c : component = false , p : componentProps = {} , child = null} = props
    if(component && showComponents){
      const Component = componentHashMap[component];
      return <div onDrop={onDrop} data-gridcellname={i}  onDragOver={allowDrop} key={i} >
                <ClearButton visible={isEditor} onClick={onClear(i)}/>
                <ItemName name={i} visible={isEditor} {...{index , layout , setLayout}}/>
            {<Component {...componentProps}>{child}</Component>}

      </div>
    }
    if(isPreview){
      return   <div data-gridcellname={i} key={i} ></div>
    }
    return  <div data-gridcellname={i} onDrop={onDrop}  onDragOver={allowDrop} className='item'  key={i} >
      <DeleteButton  visible={isEditor} onClick={onDelete(i)} />
      <ItemName name={i} {...{index , layout , setLayout}}/>
    </div>
  }
  if(!initlized){
    return <div>initilizing</div>
  }
  return (
      <Fragment>
        <ComponentParamsDialog openPopUp={openPopUp} onClose={() => setOpenPopUp(null)} />
        <ReactGridLayout {...defaultProps} layout={layout} >
          {layout.map( (item , index) => renderComponent(item , index))}
        </ReactGridLayout>
      </Fragment>
  )
}

LayoutManager.propTypes = {
  layout : PropTypes.array.isRequired,
  /**
      show/hide editor tools
   **/
  isEditor : PropTypes.bool,
  /**
      hide/show grid items background
  **/
  isPreview : PropTypes.bool,
  /**
    hide/show components
  **/
  showComponents : PropTypes.bool,
  cols : PropTypes.number,
  margin : PropTypes.array,
  componentsParams : PropTypes.object,
  components : PropTypes.object,
  setLayout : PropTypes.func
}
LayoutManager.defaultProps = {
  layout : [],
  isEditor : true,
  isPreview : false,
  showComponents : true,
  margin : [5,5],
  rowHeight : 5,
  /**
    components default props
   **/
  componentsParams : {},
  /**
    availble components that will be rendered on grid
   **/
  components : {},
  /**
   fire on layout change
   **/
  setLayout : () => {}
}

export default LayoutManager