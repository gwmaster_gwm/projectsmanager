import React, { useEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import './ClearButton.css'
const ClearButton = ({onClick , visible}) => {
  if(!visible) return null
  return (<div onClick={() => onClick()} className='clear-button'><DeleteForeverIcon/></div>)
}

ClearButton.propTypes = {
    onClick : PropTypes.func,
    visible : PropTypes.bool
}
ClearButton.defaultProps = {
    onClick : () => {},
    visible : true
}

export default ClearButton