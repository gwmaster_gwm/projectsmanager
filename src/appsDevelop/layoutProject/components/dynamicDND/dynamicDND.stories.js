import React, {useEffect, useState} from 'react';
import '../../styles/react-grid-layout.css'
import '../../styles/react-resizable.css'
import List from '../material-ui/List'
import Header from "../material-ui/Header";
import AppBar from  '../material-ui/AppBar'
import Logo from '../basic/Logo'
import './dynamicDND.stories.css'
import ComponentsList from "./ComponentsList";
import Switch from '@material-ui/core/Switch';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import ClearButton from '../LayoutManager/ClearButton'
import _ from 'lodash'

export default {
    title: 'layoutProject'
};
const updateLayoutData = [{"i":"logo","x":6,"y":0,"w":1,"h":24,"c":"Logo","p":{"url":"https://marketingland.com/wp-content/ml-loads/2015/11/content-marketing-idea-lightbulb-ss-1920-800x450.jpg"},"moved":false,"static":false},{"i":"navigation","x":6,"y":24,"w":1,"h":90,"moved":false,"static":false,"c":"List","minW":1,"maxW":2},{"i":"header","x":0,"y":0,"w":6,"h":13,"moved":false,"static":false,"c":"AppBar"},{"i":"content","x":0,"y":13,"w":6,"h":100,"moved":false,"static":false}]
const updateLayoutData2 = [{"i":"logo","x":2,"y":13,"w":4,"h":93,"moved":false,"static":false},{"i":"navigation","x":6,"y":13,"w":1,"h":93,"moved":false,"static":false,"minW":1,"maxW":2},{"i":"header","x":0,"y":0,"w":7,"h":13,"moved":false,"static":false},{"i":"name_1581802217549","x":0,"y":13,"w":2,"h":93,"moved":false,"static":false},{"i":"name_1581802229517","x":0,"y":106,"w":7,"h":18,"moved":false,"static":false}]
const testLayoutData = [{"i":"logo","x":0,"y":0,"w":1,"h":13,"c":"Logo","p":{"url":"https://marketingland.com/wp-content/ml-loads/2015/11/content-marketing-idea-lightbulb-ss-1920-800x450.jpg"},"moved":false,"static":false},{"i":"navigation","x":0,"y":13,"w":1,"h":132,"moved":false,"static":false,"c":"List"},{"i":"header","x":1,"y":0,"w":6,"h":13,"moved":false,"static":false,"c":"AppBar"},{"i":"content","x":1,"y":13,"w":6,"h":132,"moved":false,"static":false,"c":"Logo","p":{"url":"https://webcomicms.net/sites/default/files/clipart/135001/happy-smile-135001-6608522.jpg"}}]
// layout
import RGL, { WidthProvider } from "react-grid-layout";
import ComponentParamsDialog from "./ComponentParamsDialog";
import DeleteButton from "../LayoutManager/DeleteButton";
import ItemName from "../LayoutManager/ItemName";
import {TextField} from "@material-ui/core";
const ReactGridLayout = WidthProvider(RGL);
// const of available components
// TODO : config components list show only needed components
const COMPONENTS = {
    Logo,
    LogoSmile : {
        c : 'Logo',
        p : { url: 'https://webcomicms.net/sites/default/files/clipart/135001/happy-smile-135001-6608522.jpg' }
    },
    LogoCool: {
      c : 'Logo',
      p : { url : 'https://i.dlpng.com/static/png/6972879_preview.png' }
    },
    AppBar,
    List,
    Header,
}
// const of components that have dynamic params {param : defaultValue }
const COMPONENTS_PARAMS = {
    Logo : { url : 'https://marketingland.com/wp-content/ml-loads/2015/11/content-marketing-idea-lightbulb-ss-1920-800x450.jpg'}
}
// hash map to get component by his name
const componentHashMap = {};
Object.keys(COMPONENTS).map(key => componentHashMap[key] = COMPONENTS[key])

export const GridComponentDynamicDND = () => {
    // layout is an array of objects, see the demo for more complete usage
    const [isEditor , setIsEditor] = useState(true);
    const [isPreview , setIsPreview] = useState(false)
    const [layout , setLayout] = useState([])
    const [openPopUp , setOpenPopUp] = useState(null)
    const [cols , setCols] = useState(7)
    // TODO : find bug
    const [rowHeight , setRowHeight] = useState(5)
    const defaultProps = {
        className: isPreview ? 'preview' : "layout",
        isDraggable: isEditor,
        isResizable: isEditor,
        onLayoutChange: newLayout => updateLayout(newLayout),
        margin : [0,0],
        cols,
        rowHeight,
        items: 50,
        width : 1200,
    };
    useEffect(() => {
        setLayout(testLayoutData)
    }, [])
    const updateLayout = (newLayout) => {
        let hashMapLayout = {}
        layout.forEach(item => hashMapLayout[item.i] = item);
        let mergetLayout = newLayout.map(item => {
            let mergetItem = {...hashMapLayout[item.i] , ...item};
            return mergetItem
        });
        setLayout(mergetLayout)
    }
    const onDrop = (event) => {
        const {currentTarget : {dataset : {gridcellname}}, dataTransfer} = event
        let componentName = dataTransfer.getData("component-name").split("\n")[0];
        let newLayout = [...layout]
        let index = _.findIndex(newLayout, {i: gridcellname});
        // check if component have dynamic params to show popUp
        if(COMPONENTS_PARAMS[componentName]){
            setOpenPopUp({
                params : COMPONENTS_PARAMS[componentName],
                componentName,
                index,
                layout : newLayout,
                setLayout
            })
            return
        }
        // check if basic component or extra params
        let componentObject = componentHashMap[componentName]
        if(typeof componentObject == 'object'){
            // add components and props
            newLayout.splice(index, 1, {...newLayout[index] , ...componentObject});
        }else{
            delete newLayout[index].p // delete previews component props
            // add basic component
            newLayout.splice(index, 1, {...newLayout[index] , c : componentName});
        }
        setLayout(newLayout)
    }
    const allowDrop = (ev) => {
        if(!isEditor){
            return false
        }
        ev.preventDefault();
    }
    const onClear = (name) => () => {
        var index = _.findIndex(layout, {i : name});
        let newLayout = [...layout];
        delete newLayout[index].c; // delete component
        delete newLayout[index].p; // delete props
        setLayout(newLayout)
    }
    const onDelete = (name) => () => {
        var index = _.findIndex(layout, {i : name});
        let newLayout = [...layout]
        newLayout.splice(index, 1)
        setLayout(newLayout)
    }
    const addItem = () => {
        var newItemName = prompt("grid item name:", 'name_'+new Date().getTime());
        if(newItemName) {
            let newItems = layout.concat([{i: newItemName, x: 0, y: Infinity, w: 1, h: 4}])
            setLayout(newItems)
        }
    }
    const renderComponent = (props , index) => {
        const {i , c : component = false , p : componentProps = {} , child = null} = props
        if(component){
            const Component = componentHashMap[component];
            return  <div onDrop={onDrop}  data-gridcellname={i}  onDragOver={allowDrop} key={i} >
                        <ClearButton visible={isEditor} onClick={onClear(i)}/>
                        {<Component {...componentProps}>{child}</Component>}
                    </div>
        }
        if(isPreview){
            return   <div data-gridcellname={i} key={i} ></div>
        }
        return  <div data-gridcellname={i} onDrop={onDrop}  onDragOver={allowDrop} className='item'  key={i} >
                    <DeleteButton  visible={isEditor} onClick={onDelete(i)} />
                    <ItemName {...{name : i , index , layout , setLayout}}/>
                </div>
    }
    return (
        <div className="grid-container">
            <ComponentParamsDialog openPopUp={openPopUp} onClose={() => setOpenPopUp(null)} />
            <div className="controllTools">
                Edit Mode :
                <Switch checked={isEditor}  onChange={() => setIsEditor(!isEditor)} value="checkedB" color="primary" inputProps={{ 'aria-label': 'primary checkbox' }}/>
                Is Preview
                <Switch checked={isPreview}  onChange={() => setIsPreview(!isPreview)} value="checkedB" color="primary" inputProps={{ 'aria-label': 'primary checkbox' }}/>
                <button onClick={()=>addItem()}>add Item</button>
                <button onClick={()=>updateLayout(updateLayoutData)}>Update Layout</button>
                <button onClick={()=>updateLayout(updateLayoutData2)}>Update Layout2</button>
                <br/>
                <TextField id="standard-number" label="Cols" type="number" value={cols} onChange={({target : {value}}) => setCols(value) }/>
                {/*<TextField id="standard-number" label="Row Height" type="number" value={rowHeight} onChange={({target : {value}}) => setRowHeight(value) }/>*/}
                <TextareaAutosize className='log-text' rowsMax={4} value={ JSON.stringify(layout)}/>
            </div>
            <div className="componentsList">
                <ComponentsList components={componentHashMap}/>
            </div>
            <div className="react-grid">
                <ReactGridLayout {...defaultProps} layout={layout} >
                    {layout.map( (item , index) => renderComponent(item , index))}
                </ReactGridLayout>
            </div>
        </div>
    )
}

