import React from 'react'
import './Logo.css'
const Logo = ({url}) => (
        <div className="fill">
            <img src={url} alt='Logo'/>
        </div>
    )
export default Logo