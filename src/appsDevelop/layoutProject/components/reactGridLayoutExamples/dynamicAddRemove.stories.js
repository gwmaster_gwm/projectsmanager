import React, {useEffect, useState} from 'react';
import '../../styles/react-grid-layout.css'
import '../../styles/react-resizable.css'
import './basic.stories.css'

export default {
    title: 'layoutProject|basic'
};


import { WidthProvider, Responsive } from "react-grid-layout";
import _ from "lodash";
const ResponsiveReactGridLayout = WidthProvider(Responsive);
const AddRemoveLayout = () => {
    const [config , setConfig] = useState({
                                                className: "layout",
                                                 cols: { lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 },
                                                rowHeight: 100
                                        })
    const [items,setItems] = useState([])
    const [newCounter , setNewCounter] = useState(0)
      useEffect(() => {
          // component did mount
          setItems(
              [0, 1, 2, 3, 4].map(function(i, key, list) {
                      return {
                          i: i.toString(),
                          x: i * 2,
                          y: 0,
                          w: 2,
                          h: 2,
                          add: i === (list.length - 1)
                      };
                  }
              )
          )
        }, [])
    const createElement = (el) => {
        const removeStyle = {
            position: "absolute",
            right: "2px",
            top: 0,
            cursor: "pointer"
        };
        const i = el.add ? "+" : el.i;
        return (
            <div className='item' onDrop={drop} onDragOver={allowDrop} key={i} data-grid={el}>
                {el.add ? (
                    <span
                        className="add text"
                        onClick={onAddItem}
                        title="You can add an item by clicking here, too."
                    >
            Add +
          </span>
                ) : (
                    <span className="text">{i}</span>
                )}
                <span
                    className="remove"
                    style={removeStyle}
                    onClick={onRemoveItem(i)}
                >
          x
        </span>
            </div>
        );
    }


    const onAddItem = () => {
        /*eslint no-console: 0*/
        setItems(items.concat({
            i: "n" + newCounter,
            x: 0,
            y: 0, // puts it at the bottom
            w: 1,
            h: 1
        }))
        setNewCounter(newCounter+1)
        console.log("adding", "n" + newCounter);

    }

    // We're using the cols coming back from this to calculate where to add new items.
    const onBreakpointChange = (breakpoint, cols) => {
        // this.setState({
        //     breakpoint: breakpoint,
        //     cols: cols
        // });
    }

    const onLayoutChange = (layout) =>  {
        //this.props.onLayoutChange(layout);
       // this.setState({ layout: layout });
    }

    const onRemoveItem = (i) =>() => {
        console.log("removing", i);
        setItems(_.reject(items, { i: i }))
    }


    const allowDrop = (ev) => {
        ev.preventDefault();
    }

    const drag = (ev) => {
        ev.dataTransfer.setData("text", ev.target.id);
    }

    const drop = (ev) => {
        // move
        ev.preventDefault();
        var data = ev.dataTransfer.getData("text");
        ev.target.appendChild(document.getElementById(data));
    }

    return (
        <div>
            <button onClick={onAddItem}>Add Item</button>
            <img src="https://www.w3schools.com/html/img_w3slogo.gif" draggable="true" onDragStart={drag} id="drag1" width="88" height="31"/>
            <ResponsiveReactGridLayout
                onLayoutChange={onLayoutChange}
                onBreakpointChange={onBreakpointChange}
                {...config}
            >
                {_.map(items, el => createElement(el))}
            </ResponsiveReactGridLayout>
        </div>
    )
}

export const dynamicAddRemove = () =>  (<AddRemoveLayout/>)