import React, {useState} from 'react';
import '../../styles/react-grid-layout.css'
import '../../styles/react-resizable.css'
import './basic.stories.css'
import List from '../material-ui/List'
import Header from "../material-ui/Header";
import AppBar from  '../material-ui/AppBar'
import Logo from '../basic/Logo'

export default {
    title: 'layoutProject'
};

// layout
import RGL, { WidthProvider } from "react-grid-layout";
const ReactGridLayout = WidthProvider(RGL);
// const of available components
const COMPONENTS = {
    Logo,
    AppBar,
    List,
    Header,
}
// hash map to get component by his name
const componentHashMap = {};
Object.keys(COMPONENTS).map(key => componentHashMap[key] = COMPONENTS[key])
const renderComponent = (props) => {
    const {i , component = false , child = null} = props
    if(component){
        const Component = componentHashMap[component];
        return  <div key={i} >{<Component {...props}>{child}</Component>}</div>
    }
    return  <div className='item' key={i} >{i}</div>
}


export const GridComponentDynamic = () => {
    // layout is an array of objects, see the demo for more complete usage
    const [isEditor , setIsEditor] = useState(true)
    let layout = [
        {i: 'logo', x: 0, y: 0, w: 1, h: 2 , component : 'Logo' , url : 'https://i.ytimg.com/vi/RuOHXppAHaY/maxresdefault.jpg' }, //static: true
        {i: 'navigation', x: 0, y: 2, w: 1, h: 12, minW: 1, maxW: 2 , component : 'List'},
        {i: 'header', x: 1, y: 0, w: 6, h: 2 , component : 'AppBar'},
        {i: 'content', x: 1, y: 3, w: 6, h: 12 , component : 'Logo' , url: 'https://searchengineland.com/figz/wp-content/seloads/2018/07/awesome-content-shutterstock_594993329-800x533.jpg' }
    ];
    if(!isEditor){
        layout = layout.map(item => ({...item , static: true  }) )
    }
    return (
        <ReactGridLayout margin={isEditor ? [5,5] : [0,0]} className="layout" layout={layout} cols={7} rowHeight={30} width={1200}>
            {layout.map(item => renderComponent(item))}
        </ReactGridLayout>
    )
}