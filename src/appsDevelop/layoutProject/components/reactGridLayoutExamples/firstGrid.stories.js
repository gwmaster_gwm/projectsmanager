import React from 'react';
import '../../styles/react-grid-layout.css'
import '../../styles/react-resizable.css'
import './basic.stories.css'

export default {
    title: 'layoutProject'
};

import RGL, { WidthProvider } from "react-grid-layout";
const ReactGridLayout = WidthProvider(RGL);
class MyFirstGrid extends React.Component {
    render() {
        // layout is an array of objects, see the demo for more complete usage
        const layout = [
            {i: 'logo', x: 0, y: 0, w: 1, h: 1, static: true},
            {i: 'navigation', x: 0, y: 1, w: 1, h: 11, minW: 1, maxW: 6},
            {i: 'header', x: 1, y: 0, w: 6, h: 1},
            {i: 'content', x: 1, y: 1, w: 6, h: 11}
        ];
        return (
            <ReactGridLayout className="layout" layout={layout} cols={7} rowHeight={30} width={1200}>
                <div className='item' key="logo">logo</div>
                <div className='item' key="header">header</div>
                <div className='item' key="navigation">navigation</div>
                <div className='item' key="content">content</div>
            </ReactGridLayout>
        )
    }
}

export const myFirstGrid = () => (<MyFirstGrid/>)