import React, {useEffect, useState} from 'react';
import '../../styles/react-grid-layout.css'
import '../../styles/react-resizable.css'
import './basic.stories.css'

export default {
    title: 'layoutProject|basic'
};

const DragItem = () => {

}
const DropArea = () => {

}
const DeleteArea = () => {

}

const DNDClone = () => {
    const [items , setItems] = useState([])
    const [counter , setCounter] = useState(0)
    const cloneItem = () => {
        setCounter(counter+1);
        return  <img src="https://www.w3schools.com/html/img_w3slogo.gif" key={counter} id={`img-${counter}`}  draggable="true" onDragStart={drag} width="88" height="31"/>
    }
    const allowDrop = (ev) => {
        ev.preventDefault();
    }
    const drag = (ev) => {
        ev.dataTransfer.setData("text", ev.target.id);
    }

    const drop = (ev) => {
        // move
        // ev.preventDefault();
        // var data = ev.dataTransfer.getData("text");
        // ev.target.appendChild(document.getElementById(data));
        // clone
        ev.preventDefault();
        const clonedItem = cloneItem();
        setItems([...items,clonedItem])
        ev.target.appendChild(clonedItem);
        ev.stopPropagation();
        return false;
    }
    const onDelete = (ev) => {
        // delete
    }
    return (
        <div>
            <img src="https://www.w3schools.com/html/img_w3slogo.gif" draggable="true" onDragStart={drag} id="original" width="88" height="31"/>
            <div onDrop={drop} onDragOver={allowDrop}  style={{width : 50 , height : 50 , backgroundColor : 'red'}}>
                Delete
            </div>
            <div onDrop={drop} onDragOver={allowDrop} style={{width : 300 , height : 300 , backgroundColor : 'blue'}}>
                Drop box 1
                {items}
            </div>
            <div onDrop={onDelete} onDragOver={allowDrop} style={{width : 300 , height : 300 , backgroundColor : 'green'}}>
                Drop box 2
                {items}
            </div>

        </div>
    )
}

export const dndClone = () =>  (<DNDClone/>)