import { Responsive as ResponsiveGridLayout } from 'react-grid-layout';
import React from "react";

export default {
    title: 'layoutProject'
};
class MyResponsiveGrid extends React.Component {
    render() {
        // {lg: layout1, md: layout2, ...}
        const layouts = {lg :[{"i":"1","x":0,"y":0,"w":5,"h":2},{"i":"2","x":10,"y":0,"w":5,"h":2},{"i":"3","x":20,"y":0,"w":5,"h":2}]};
        return (
            <ResponsiveGridLayout className="layout" layouts={layouts}
                                  breakpoints={{lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0}}
                                  cols={{lg: 12, md: 10, sm: 6, xs: 4, xxs: 2}}>
                <div key="1">1</div>
                <div key="2">2</div>
                <div key="3">3</div>
            </ResponsiveGridLayout>
        )
    }
}
export const myResponsiveGrid = () => (
    <MyResponsiveGrid/>
)