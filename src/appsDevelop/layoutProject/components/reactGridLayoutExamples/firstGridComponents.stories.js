import React from 'react';
import '../../styles/react-grid-layout.css'
import '../../styles/react-resizable.css'
import './basic.stories.css'
import Button from '@material-ui/core/Button';
import List from '../material-ui/List'
import Header from "../material-ui/Header";
import AppBar from  '../material-ui/AppBar'
import Logo from '../basic/Logo'

export default {
    title: 'layoutProject'
};

import RGL, { WidthProvider } from "react-grid-layout";

const ReactGridLayout = WidthProvider(RGL);
class MyFirstGridComponents extends React.Component {

    render() {
        // layout is an array of objects, see the demo for more complete usage
        const layout = [
            {i: 'logo', x: 0, y: 0, w: 1, h: 2}, //static: true
            {i: 'navigation', x: 0, y: 1, w: 1, h: 12, minW: 1, maxW: 2},
            {i: 'header', x: 1, y: 0, w: 6, h: 2},
            {i: 'content', x: 1, y: 3, w: 6, h: 12}
        ];
        return (
            <ReactGridLayout margin={[2,2]} className="layout" layout={layout} cols={7} rowHeight={30} width={1200}>
                <div className='item' key="logo"><Logo/></div>
                <div key="header"><AppBar/></div>
                <div className='item' key="navigation"><List/></div>
                <div className='item'  key="content"><Button color='secondary'>Button</Button></div>
            </ReactGridLayout>
        )
    }
}

export const myFirstGridComponents = () => (<MyFirstGridComponents/>)