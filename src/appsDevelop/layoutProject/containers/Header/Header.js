import React, { useEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types';
import Menu from "./Menu/Menu";

const Header = (props) => {
  return (<div className='header-component'>
    <Menu/>
  </div>)
}

Header.propTypes = {}
Header.defaultProps = {}

export default Header