import React, { useEffect, useRef, useState } from 'react'
import {Icon} from 'antd'
import { Menu, Dropdown, Button } from 'antd';
import {ROUTES} from "../../../stores/constants";
const {PAGES} = ROUTES

import {connect} from 'react-redux'
import {mapActionsToProps, mapStoreToProps} from "fast-redux-reducer";
import {RoutesActions} from "../../../stores/actions";

const MyMenu = (props) => {
    // reducer
    const {router : {location : {pathname , hash , search}}} = props;
    // actions
    const {navigate} = props

    const handleClick = ({ item, key, keyPath, selectedKeys, domEvent }) => {
        navigate(key)
    }
    const getName = (path) => {
        return path.split("/").pop()
    }
    const menu = () => (
        <Menu
            onClick={handleClick}
            selectedKeys={[pathname+search]}
        >
            <Menu.Item key={PAGES.HOME}>Home</Menu.Item>
            <Menu.Item key={PAGES.LAYOUT_CONFIGURATION}>Layout Config</Menu.Item>
            <Menu.Item key={PAGES.LAYOUTS}>Layouts</Menu.Item>
            <Menu.Item key={PAGES.SAVED}>Saved</Menu.Item>
        </Menu>
    )
    return (
        <div className='menu'>
            <Dropdown overlay={menu} placement="bottomLeft">
                <Icon type="menu" />
            </Dropdown>
        </div>
    )
}

export default connect(
    mapStoreToProps('router'),
    mapActionsToProps(RoutesActions )
)(MyMenu)