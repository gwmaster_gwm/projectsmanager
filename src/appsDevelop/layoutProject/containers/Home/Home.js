import React from 'react'

import {connect} from 'react-redux'
import {mapStoreToProps, mapActionsToProps} from 'fast-redux-reducer'
import LayoutManager from '../../components/LayoutManager/LayoutManager'
import {LayoutConfigurationAction,LayoutsActions} from '../../stores/actions'
const Home = (props) => {
  // redcuer
  const {
    layoutConfiguration ,
    layoutConfigurationEditMode,
    layoutConfigurationShowComponents,
    layoutConfigurationComponents,
    layoutConfigurationComponentsParams
  } = props
  const {
    layout = [],
  } = layoutConfiguration

  return (
        <div className="configuration">
          <LayoutManager layout={layout} margin={[0,0]} isEditor={false} showComponents={true} isPreview={true} components={layoutConfigurationComponents} componentsParams={layoutConfigurationComponentsParams} />
        </div>
  )
}

export default connect(
    mapStoreToProps('layoutConfiguration','layoutConfigurationEditMode','layoutConfigurationShowComponents','layoutConfigurationComponents','layoutConfigurationComponentsParams'),
    mapActionsToProps(LayoutConfigurationAction , LayoutsActions )
)(Home)