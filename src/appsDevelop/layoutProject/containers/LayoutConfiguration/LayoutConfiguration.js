import React, {useEffect, useRef, useState} from 'react'
import PropTypes from 'prop-types';
import {connect} from 'react-redux'
import {mapStoreToProps, mapActionsToProps} from 'fast-redux-reducer'
import LayoutManager , {clearLayout} from '../../components/LayoutManager/LayoutManager'
import {LAYOUT_CONFIGURATION} from './LayoutConfiguration.const'
import {LayoutConfigurationAction,LayoutsActions} from '../../stores/actions'
import ComponentsList from "../../components/LayoutManager/ComponentsList";

// icons
import Save from '@material-ui/icons/Save'
import AddToQueueIcon from '@material-ui/icons/AddToQueue';
import QueueIcon from '@material-ui/icons/Queue';
import EditIcon from '@material-ui/icons/Edit';
import LayersIcon from '@material-ui/icons/Layers';
import LayersClearIcon from '@material-ui/icons/LayersClear';
import ImportExportIcon from '@material-ui/icons/ImportExport';
import Delete from '@material-ui/icons/Delete'

const LayoutConfiguration = (props) => {
    // redcuer
    const {
        layoutConfiguration ,
        layoutConfigurationEditMode,
        layoutConfigurationShowComponents,
        layoutConfigurationComponents,
        layoutConfigurationComponentsParams
    } = props
    const {
        layout = [],
        pageName,
        layoutName ,
    } = layoutConfiguration
    const layoutConfigurationUblicate = JSON.parse(JSON.stringify(layoutConfiguration))
    // actions
    const {setLayout , setEditMode , setShowComponents, addItem ,   savePage,  saveLayout} = props
    const onLayoutChange = (newLayout) => {
        setLayout({...layoutConfigurationUblicate , layout : newLayout})
    }
    const saveLayoutHandler = () => {
        var newLayoutName = prompt("Save Layout", layoutName);
        if(!newLayoutName) return
        setLayout({...layoutConfigurationUblicate , layoutName : newLayoutName})
        saveLayout({layout : clearLayout(layout) , layoutName : newLayoutName})
    }
    const savePageHandler = () => {
        var newPageNanme = prompt("Save Layout", pageName);
        if(!newPageNanme) return
        setLayout({...layoutConfigurationUblicate , pageName : newPageNanme})
        savePage({...layoutConfigurationUblicate , pageName : newPageNanme})
    }
    const addItemHandler = () => {
        var itemName = prompt("Item name", '');
        if(!itemName) return
        addItem(itemName)
    }
    const importExport = () => {
        var newLaout = prompt("Import/Export", JSON.stringify(layoutConfiguration));
        if(!newLaout) return
        setLayout(JSON.parse(newLaout))
    }
    const deleteHandler = () => {
        if (confirm("Delete All?")) {
            setLayout({})
        }
    }
    return (
        <div className="grid-container-configuration">
            <div className="components-title">
                Components
            </div>
            <div className="components">
                <ComponentsList components={layoutConfigurationComponentsParams}/>
            </div>
            <div className="configuration-title">
                Configuration Screen - {pageName}({layoutName})
                <div className="configuration-buttons">
                    <span title='Save Page' onClick={deleteHandler} className='button warning'><Delete/></span>
                    |
                    <span title='Save Page' onClick={savePageHandler} className='button'><Save/></span>
                    <span title='Save Layout' onClick={saveLayoutHandler}  className='button'><AddToQueueIcon/></span>
                    <span title='Import/Export' onClick={importExport}  className='button'><ImportExportIcon/></span>
                    |
                    <span title='Edit Mode' onClick={() => setEditMode(!layoutConfigurationEditMode)} className={`button ${layoutConfigurationEditMode ? 'selected' : ''}`}><EditIcon/></span>
                    <span title='Components Layer' onClick={() => setShowComponents(!layoutConfigurationShowComponents)} className={`button ${layoutConfigurationShowComponents ? 'selected' : ''}`}>{layoutConfigurationShowComponents ? <LayersIcon/> : <LayersClearIcon/> }</span>
                    |
                    <span title='add item' onClick={() => addItemHandler()} className='button succses'><QueueIcon/></span>
                </div>
            </div>
            <div className="configuration">
                <LayoutManager layout={layout} margin={[0,0]} isEditor={layoutConfigurationEditMode} showComponents={layoutConfigurationShowComponents} isPreview={!layoutConfigurationEditMode} setLayout={onLayoutChange} components={layoutConfigurationComponents} componentsParams={layoutConfigurationComponentsParams} />
            </div>
        </div>
    )
}


export default connect(
    mapStoreToProps('layoutConfiguration','layoutConfigurationEditMode','layoutConfigurationShowComponents','layoutConfigurationComponents','layoutConfigurationComponentsParams'),
    mapActionsToProps(LayoutConfigurationAction , LayoutsActions )
)(LayoutConfiguration)