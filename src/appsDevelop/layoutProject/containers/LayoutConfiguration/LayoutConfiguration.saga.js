import { all, call, put, select, takeLatest } from 'redux-saga/effects'
import { LAYOUT_CONFIGURATION } from './LayoutConfiguration.const'
import {updateLayout,addItem} from '../../components/LayoutManager/LayoutManager'

function * setLayout({payload : newLayout}) {
    let {
        layoutConfiguration
    } = yield select(state => state)
    debugger
    // receive layout and apply on current page
    yield put({ type: LAYOUT_CONFIGURATION.SET_CONFIGURATION_LAYOUT, payload : updateLayout(layoutConfiguration,newLayout) })
}
function * addItemHandler({payload : name}) {
    let {
        layoutConfiguration
    } = yield select(state => state)
    const {layout} = layoutConfiguration
    yield put({
        type: LAYOUT_CONFIGURATION.SET_CONFIGURATION_LAYOUT,
        payload : {
                 ...layoutConfiguration,
                layout : addItem(layout , name)
            }
    })
}
function * applyLayout({payload : newLayout}) {
    let {
        layoutConfiguration
    } = yield select(state => state)
    // receive layout and apply on current page
    yield put({ type: LAYOUT_CONFIGURATION.SET_CONFIGURATION_LAYOUT, payload :  {...layoutConfiguration , layout : updateLayout(layoutConfiguration.layout,newLayout)}})
}


export default function * () {
  yield takeLatest(LAYOUT_CONFIGURATION.SET_LAYOUT_SAGA, setLayout)
  yield takeLatest(LAYOUT_CONFIGURATION.APPLY_LAYOUT_SAGA , applyLayout)
  yield takeLatest(LAYOUT_CONFIGURATION.ADD_ITEM_SAGA, addItemHandler)
}
