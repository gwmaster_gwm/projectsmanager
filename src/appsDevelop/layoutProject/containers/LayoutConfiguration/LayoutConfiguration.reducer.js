import { LAYOUT_CONFIGURATION } from './LayoutConfiguration.const'
/*
    Mast add components that will use !!
 */
import Logo from "../../components/basic/Logo";
import AppBar from "../../components/material-ui/AppBar";
import List from "../../components/material-ui/List";
import Header from "../../components/material-ui/Header";
import LineBuffer from '../../components/material-ui/LineBuffer'
import Grid from '../../components/material-ui/Grid'
const COMPONENTS = {
  Logo,
  AppBar,
  List,
  Header,
  LineBuffer,
  Grid
}
// const of components that have dynamic params {param : defaultValue }
// TODO : build dynamic params system
const COMPONENTS_PARAMS = {
  Logo : {
    c : 'Logo',
    url : 'https://image.freepik.com/free-vector/cool-monkey-logo-design-vector-illustrator_56473-420.jpg',
    dialog : true
  },
  LogoSmile : {
    c : 'Logo',
    p : { url: 'https://webcomicms.net/sites/default/files/clipart/135001/happy-smile-135001-6608522.jpg' }
  },
  LogoCool: {
    c : 'Logo',
    p : { url : 'https://www.coolaccidents.com//sites/g/files/g2000006916/files/2018-02/CoolAccidents-OGImage.jpg' }
  },
  AppBar,
  List,
  Header,
  LineBuffer,
  Grid
}

const exampleLayout = {"layout":[{"i":"logo 22","x":0,"y":0,"w":2,"h":27,"c":"Logo","p":{"url":"https://www.coolaccidents.com//sites/g/files/g2000006916/files/2018-02/CoolAccidents-OGImage.jpg"},"moved":false,"static":false},{"i":"navigation","x":0,"y":27,"w":2,"h":102,"moved":false,"static":false,"c":"List"},{"i":"header","x":2,"y":0,"w":10,"h":24,"moved":false,"static":false,"c":"Header"},{"i":"content","x":2,"y":24,"w":10,"h":105,"moved":false,"static":false,"c":"Grid"}],"layoutName":"myLayout","pageName":"Page"};


export default {
  layoutConfigurationComponents : [LAYOUT_CONFIGURATION.SET_COMPONENTS , COMPONENTS],
  layoutConfigurationComponentsParams : [LAYOUT_CONFIGURATION.SET_COMPONENTS_PARAMS, COMPONENTS_PARAMS],
  layoutConfiguration : [LAYOUT_CONFIGURATION.SET_CONFIGURATION_LAYOUT , exampleLayout],
  layoutConfigurationEditMode : [LAYOUT_CONFIGURATION.SET_EDIT_MODE , true],
  layoutConfigurationShowComponents : [LAYOUT_CONFIGURATION.SET_SHOW_COMPONENTS , true],
}


