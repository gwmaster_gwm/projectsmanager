import { LAYOUT_CONFIGURATION } from './LayoutConfiguration.const'
// on layout change update layout
const setLayout = payload => ({ type: LAYOUT_CONFIGURATION.SET_CONFIGURATION_LAYOUT, payload })
const applyLaout = payload => ({ type: LAYOUT_CONFIGURATION.APPLY_LAYOUT_SAGA, payload })
const setEditMode = payload => ({ type: LAYOUT_CONFIGURATION.SET_EDIT_MODE, payload})
const setShowComponents = payload => ({ type: LAYOUT_CONFIGURATION.SET_SHOW_COMPONENTS, payload})
const addItem = payload => ({ type: LAYOUT_CONFIGURATION.ADD_ITEM_SAGA, payload})
export const LayoutConfigurationAction = {
  setLayout,
  setEditMode,
  setShowComponents,
  applyLaout,
  addItem
}