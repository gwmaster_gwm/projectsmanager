import { LAYOUTS } from './Layouts.const'

const exampleSaved = {
  "Example-1" : {"layout":[{"i":"logo 22","x":0,"y":0,"w":2,"h":25,"c":"Logo","p":{"url":"https://webcomicms.net/sites/default/files/clipart/135001/happy-smile-135001-6608522.jpg"},"moved":false,"static":false},{"i":"navigation","x":0,"y":25,"w":2,"h":116,"moved":false,"static":false,"c":"List"},{"i":"header","x":2,"y":0,"w":10,"h":13,"moved":false,"static":false,"c":"AppBar"},{"i":"content","x":2,"y":13,"w":10,"h":116,"moved":false,"static":false,"c":"Logo","p":{"url":"https://image.freepik.com/free-vector/cool-monkey-logo-design-vector-illustrator_56473-420.jpg"}}],"layoutName":"examplePage","pageName":"Example1"},
  "Example-2" : {"layout":[{"i":"logo 22","x":0,"y":0,"w":2,"h":27,"c":"Logo","p":{"url":"https://www.coolaccidents.com//sites/g/files/g2000006916/files/2018-02/CoolAccidents-OGImage.jpg"},"moved":false,"static":false},{"i":"navigation","x":0,"y":27,"w":2,"h":102,"moved":false,"static":false,"c":"List"},{"i":"header","x":2,"y":0,"w":10,"h":24,"moved":false,"static":false,"c":"Header"},{"i":"content","x":2,"y":24,"w":10,"h":105,"moved":false,"static":false,"c":"Grid"}],"layoutName":"myLayout","pageName":"Example2"}
}

const exampleLayout = {
  "Template-1": [{"i":"logo 22","x":0,"y":0,"w":2,"h":25,"moved":false,"static":false},{"i":"navigation","x":0,"y":25,"w":2,"h":101,"moved":false,"static":false},{"i":"header","x":2,"y":0,"w":10,"h":13,"moved":false,"static":false},{"i":"content","x":2,"y":13,"w":10,"h":113,"moved":false,"static":false}],
  "Template-2": [{"i":"logo 22","x":0,"y":0,"w":2,"h":25,"moved":false,"static":false},{"i":"navigation","x":10,"y":13,"w":2,"h":101,"moved":false,"static":false},{"i":"header","x":2,"y":0,"w":10,"h":13,"moved":false,"static":false},{"i":"content","x":0,"y":25,"w":10,"h":89,"moved":false,"static":false},{"i":"extra","x":2,"y":13,"w":8,"h":12,"moved":false,"static":false}]
}





function layoutSavedLayouts (state = exampleLayout, { type, payload }) {
  switch(type){
    case LAYOUTS.SET_SAVED_LAYOUTS:
      const {layout , layoutName} = payload
      return {
        ...state,
        [layoutName] : layout
      }
    case LAYOUTS.DELETE_SAVED_LAYOUT:
      const newState = {...state}
      delete newState[payload]
      return newState
  }
  return state
}

function layoutSavedPages (state = exampleSaved, { type, payload }) {
  switch (type) {
    case LAYOUTS.SET_SAVED_PAGES:
      const {pageName} = payload
      return {
        ...state,
        [pageName] : payload
      }
    case LAYOUTS.DELETE_SAVED_PAGE:
      const newState = {...state}
      delete newState[payload]
      return newState
  }

  return state
}

export default {
  layoutSavedLayouts,
  layoutSavedPages,
}
