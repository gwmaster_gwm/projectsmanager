import React, {useEffect, useRef, useState} from 'react'
import PropTypes from 'prop-types';
import {connect} from 'react-redux'
import {mapStoreToProps, mapActionsToProps} from 'fast-redux-reducer'

import {ROUTES} from '../../stores/constants'
import {LayoutsActions , LayoutConfigurationAction , RoutesActions} from '../../stores/actions'
import AddToQueueIcon from "@material-ui/icons/AddToQueue";
import Delete from '@material-ui/icons/Delete'

import LayoutManager from "../../components/LayoutManager/LayoutManager";
function Layouts(props) {
    // redcuer
    const {layoutSavedLayouts , layoutSavedPages , layoutConfigurationComponents} = props
    // actions
    const {deletePage, deleteLayout , setLayout , navigate , applyLaout} = props
    // router
    const {router : {location : {pathname , search , hash}}} = props;
    const isSaved = search.includes('saved')
    const loadPageHandler = (name) => {
        setLayout(layoutSavedPages[name])
        navigate(ROUTES.PAGES.LAYOUT_CONFIGURATION)
    }
    const applyLayoutHandler = (name) => {
        applyLaout(layoutSavedLayouts[name])
        navigate(ROUTES.PAGES.LAYOUT_CONFIGURATION)
    }
    const LayoutBox = ({name, layout}) => (
        <div className='item'>
            <div onClick={() => applyLayoutHandler(name)} className='button' >Apply</div>
            <span className='item-title'>{name}</span>
            <LayoutManager className='render-content' isEditor={false} rowHeight={2} showComponents={true} layout={layout} />
            <div onClick={() => deleteLayout(name)} className='item-delete-button'><Delete/></div>
        </div>
    )
    const PageBox = ({name, layout}) => (
        <div className='item'>
            <div onClick={() => loadPageHandler(name)} className='button'>Load</div>
            <span className='item-title'>{name}</span>
            <LayoutManager className='render-content' style={{zoom : 0.4}} components={layoutConfigurationComponents}  rowHeight={5} isEditor={false} layout={layout} />
            <div onClick={() => deletePage(name)} className='item-delete-button'><Delete/></div>
        </div>
    )
    return (
        <div className="grid-container-layouts">
            <div className="title">
                {isSaved ? 'Saved Screen' : 'Layout Screen'}
                <div className="control">
                    <AddToQueueIcon/>
                </div>
            </div>
            <div className='grid'>
                {isSaved ?
                    Object.keys(layoutSavedPages).map(name => <PageBox key={name} name={name} layout={layoutSavedPages[name].layout}/>)
                    :
                    Object.keys(layoutSavedLayouts).map(name => <LayoutBox key={name} name={name} layout={layoutSavedLayouts[name]}/>)
                }
            </div>
        </div>
    )
}

Layouts.propTypes = {};
Layouts.defaultProps = {};

export default connect(
    mapStoreToProps('router','layoutSavedLayouts' , 'layoutSavedPages','layoutConfigurationComponents'),
    mapActionsToProps(LayoutsActions , LayoutConfigurationAction , RoutesActions)
)(Layouts)