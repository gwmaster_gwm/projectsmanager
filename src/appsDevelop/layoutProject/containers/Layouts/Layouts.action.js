import { LAYOUTS } from './Layouts.const'
const savePage = payload => ({ type: LAYOUTS.SET_SAVED_PAGES, payload })
const saveLayout = payload => ({ type: LAYOUTS.SET_SAVED_LAYOUTS, payload })
const deletePage = payload => ({ type: LAYOUTS.DELETE_SAVED_PAGE, payload })
const deleteLayout = payload => ({ type: LAYOUTS.DELETE_SAVED_LAYOUT, payload })
export const LayoutsActions = {
  savePage,
  saveLayout,
  deletePage,
  deleteLayout
}