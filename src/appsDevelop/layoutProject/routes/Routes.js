import React , {Fragment} from 'react'
import { Route, Switch } from 'react-router'
import Header from '../containers/Header/Header'
import Home  from '../containers/Home/Home'
import LayoutConfiguration from '../containers/LayoutConfiguration/LayoutConfiguration'
import {ROUTES} from '../stores/constants'
import Layouts from "../containers/Layouts/Layouts";
const {PAGES} = ROUTES


const NoMatch = () => (
    <div>
        No Match
    </div>
)

const routes = (
    <div className="routes-container">
        <div className="header">
            <Header />
        </div>
        <div className="content">
            <div className='content-card'>
                <Switch>
                    <Route exact path={PAGES.HOME} component={Home} />
                    <Route exact path={PAGES.LAYOUT_CONFIGURATION} component={LayoutConfiguration} />
                    <Route exact path={PAGES.LAYOUTS} component={Layouts} />
                    <Route component={NoMatch} />
                </Switch>
            </div>
        </div>
    </div>
)

export default routes