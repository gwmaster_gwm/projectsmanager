const pr = 'ROUTES_'

const ROOT = ''
const PAGES = {
  HOME : `${ROOT}/`,
  LAYOUTS : `${ROOT}/layouts`,
  SAVED : `${ROOT}/layouts?saved`,
  LAYOUT_CONFIGURATION : `${ROOT}/layouts-configuration`
}


export const ROUTES = {
  NAVIGATE_SAGA: `${pr}NAVIGATE_SAGA`,
  PAGES
}